# ExG Sensor Project

## Introduction

The ExG Sensor Project was initiated by the Sensor Technology Research Centre at the University of Sussex with the intention of allowing more 
people to develop applications using its patented Electric Potential Sensor (EPS) technology. This has been achieved by re-designing the EPS device 
so that it now comes in a "plug in and play" format which works either directly with the microcontroller or via the addition of a simple 
Analogue to Digital converter (ADC) device.
The ExG Sensor can then be used to make a wide range of electrophysiological readings and can also be used to measure movement and gestures.

## Getting Started

The ExG Sensor can be contected to a microcontroller in two ways, both are illustrated in the ExG Sensor User Manual which is included with the 
download files.

### Microcontroller with built-in differential ADC (e.g. w/ Teensy 3.1)

A pair of ExG Sensors can be conntected to a Teensy 3.1 microcontroller using the following connections

Teesny 3.1                | ExG Sensor 1   | ExG Sensor 2
:------------------------ | :------------- |:------------
V in                      |  VCC           | VCC
Analog Ground (AGND)      |  GND           | GND
A 10                      |  SIG           | 
A 11                      |                | SIG

### Interfacing with a microcontroller via an external ADC (e.g. w/ Arduino Uno)

An Arduino board (or similar which has I2C capability) is used to acquire the data of an external ADC, for example an ADS1115 programmable ADC board. 
A pair ExG nodes can be connected to an Arduino Uno with the ADS1115 ADC in the following manner.

Arduino UNO              | ADS 1115      | ExG Sensor 1   | ExG Sensor 2
:------------------------|:------------- | :------------- |:------------
5 V                      | VDD           |  VCC           | VCC
GND                      | GND           |  GND           | GND
SCL                      | SCL           |  SIG           | 
SDA                      | SDA           |                | SIG
                         | A0            |  SIG           | 
                         | A1            |                | SIG
						 
###  Microcontroller programming

The basic example programs that can be used with the microcontrollers depicted in the previous section are listed in the table below. The programs 
are included in the Microcontroller Examples folder of the download files, they can be uploaded to the microcontroller using the Arduino IDE.

Microcontroller                                           | Program            
:-------------------------------------------------------- | :-------------------------
Built in Differential ADC (e.g. Teensy 3.1)               |  ExG_Teensy_analogRead_V1          
Exteranl ADC (e.g. Arduino Uno or other I2C compatible)   |  ExG_Arduino_analogRead_V1     

### Data readouts

In order to visualise the data received from the ExG Sensors via the microcontroller the user can either use the Serial Plotter 
in the Tools tab of the Arduino IDE or the C# GUI provided. The C# GUI is entitled ExG_Sensor_GUI.exe and is contained in the folder 
marked C# GUI. It can be downloaded on to a Windows PC or Laptop and it is a standalone executable file.

## ExG Sensor Functionality

The ExG Sensor is a versatile electrophysiological sensor system compatible with Arduino, Teensy, LilyPad, and other microcontroller platforms. 
It allows the user to measure equally well ECG (heart rate), EOG (eye movements) or EMG (muscle activation), depending on where it is placed on 
the body. 

In conjunction with the electrophysiological sensing the ExG Sensor can also be used to measure movement or gestures made by the hand of the subject 
at close range. The movement of a conductive object, e.g. a human hand, causes changes to the local electric field which the ExG Sensor can detect 
and convert to a digital response. 

Both of these functionalities are illustrated in the ExG Sensor User Manual which is included with the download files.

### Contributors

Many thanks to the following people who have contributed to this project

* Prof R Prance

* Mr Martin Nock

* Dr Daniel Roggen

* Mr Arash Pour Yazdan Panah Kermani


