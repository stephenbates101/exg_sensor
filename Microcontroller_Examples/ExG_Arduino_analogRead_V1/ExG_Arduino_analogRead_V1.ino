/*
 * Basic Arduino Program to Use with initial ExG Sensor
 * Device - Arduino Uno (or similar)
 * Serial Port USB, baud rate 115200
 * ADS1115 16bit ADC
 * 
 * Program created by Stephen Bates
 * 19th April 2018
 * STRC
 * University of Sussex
*/

//Include Arduino file for I2C
#include <Wire.h>
//Include file for ADC driver
#include <Adafruit_ADS1015.h>
//Start ADS1115
Adafruit_ADS1115 ads1115(0x48);

//Define serialPort to be used
//This version uses the Serial port which
//this is the USB port
#define serialPort Serial

// Introduce Variables
int Value;

void setup() {
  
  // set up ads1115
  ads1115.begin();
  
  //set gain level
  ads1115.setGain(GAIN_ONE); // 1x gain +/- 4.096 Volts
  
  // Initialise the serial port
  serialPort.begin(115200);
}


void loop() {

  // Read rawValue from ADC
  Value = ads1115.readADC_Differential_0_1();
  
  // Send out data
  serialPort.println(Value);
  
}
