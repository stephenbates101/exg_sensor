/*
 * Basic Teensy Program to Use with initial ExG Sensor
 * Device - Teensy 3.1 (or later)
 * Serial Port USB, baud rate 115200
 * 
 * Program created by Stephen Bates
 * 19th April 2018
 * STRC
 * University of Sussex
*/
// Include the Teensy ADC file
#include <ADC.h>

// Inialise the ADC
ADC *adc = new ADC(); // adc object;

//Define serialPort to be used
//This version uses the Serial port which
//this is the USB port
#define serialPort Serial

void setup() {

    // Initialise the pins for the ADC readings
    pinMode(A10, INPUT); //Diff Channel 0 Positive
    pinMode(A11, INPUT); //Diff Channel 0 Negative

    // Initialise the serial port
    serialPort.begin(115200);

    adc->setResolution(16); // set bits of resolution

    adc->enablePGA(4,0); // enable and set the progamable gain

    
    adc->setConversionSpeed(ADC_CONVERSION_SPEED::HIGH_SPEED); // change the conversion speed // dan use this, works well
  
    adc->setSamplingSpeed(ADC_SAMPLING_SPEED::VERY_HIGH_SPEED); // change the sampling speed 

}

// Iniitalise the variables
int Value;
int readingNumber = 2000;

void loop() {

    // Reset the global variable and carry out readingd
    Value=0;
    for(int i=0;i<readingNumber;i++)
    {
      Value += adc->adc0->analogReadDifferential(A10, A11); // read a new value
    }
    // Take the mean value 
    Value /= readingNumber;
    // Send the data out via the serial port
    serialPort.println(Value);
    
}


